/*
В качестве параметров в функцию передается: x и абсолютная погрешность вычислений.
Все параметры и возвращаемое значение функции имеют вещественный тип. Используя
разработанную функцию вывести таблицу значений на промежутке [A,B] с шагом H.
Точность расчета задается пользователем.*/
#include <stdlib.h>
#include <locale.h>
#include <stdio.h>
#include <math.h>
double dcos(double , double );
int main(int argc, char *argv[])
{
	setlocale(LC_ALL, "RU");
	double N;
	float a,b,h,i;
	printf("Введите A и B : ");
	scanf("%f%f",&a,&b);
	printf("Введите Шаг : ");
	scanf("%f",&h);
	printf("Введите погрешность : ");
	scanf("%lf",&N);
    double 	n=b/N;
	//printf("%lf",n);
	for(i=a;i<b;i+=h){
		//dcos(i,n);
		printf("dcos %.3f = %.3lf cos = %.3lf\n", i, dcos(i,n), cos(i) );
	}
	return 0;
}
int fak(int i){
	int fak=1;
		while(i>1){
        fak*=i;
        i--;
    }
	return fak;
}
double dcos(double x, double n){
	double dcos=0;
	int i;

	for ( i = 0 ; i  < n ; i++)
	{
		dcos+=pow(-1,i)*1.0*(pow(x,2*i)/fak(2*i));
	}
	return dcos;
}
//void print(
