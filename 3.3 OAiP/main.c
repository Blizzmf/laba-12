/*Реализовать функцию SortArray, осуществляющую сортировку элементов вещественного
массива, расположенных на четных позициях, по возрастанию, а на нечетных – по
убыванию (или наоборот). Массив, его размер и направление сортировки передаются в
качестве параметров. Используя разработанную функцию обработать N массивов,
введенных пользователем.*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    srand(time(NULL));
    int q=0,i,N,n,temp;
    double *a;
    printf("Введите кол-во массивов : ");
    scanf("%d",&N);
    do{
        printf("Введите размер массива : ");
        scanf("%d",&n);
        a=(double*)calloc(n,sizeof(double));
        for(i=0;i<n;i++){
        a[i]=rand()/100000000.0;
        printf("%lf ",a[i]);

        }
        SortArray(a,n);
        printf("\n");
        for(i=0;i<n;i++){
            printf("%lf ",a[i]);
        }
        q++;
        free(a);
       }
    while(q<N);

    return 0;
}
void SortArray(double *a, int n){
    int i,j;
    double temp;
    for(i=0;i<n;i++){
        for(j=0;j<n-i-2;j+=2){
            if(a[j]>a[j+2]){
                temp=a[j];
                a[j]=a[j+2];
                a[j+2]=temp;
            }
        }
    }
    for(i=0;i<n;i++){
        for(j=1;j<n-2;j+=2){
            if(a[j]<a[j+2]){
                temp=a[j];
                a[j]=a[j+2];
                a[j+2]=temp;
            }
        }
    }
}
