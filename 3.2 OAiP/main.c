/*Реализовать функцию вычисления периметра и площади прямоугольного треугольника. В
качестве параметров передаются длины катетов треугольника. Используя разработанную
функцию вычислить периметры и площади N треугольников, длины катетов которых
вводятся пользователем.*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
typedef struct{
    double sqr;
    double perim;
}Schet;
Schet* square(int a, int b,Schet* sc);
int main()
{
    int i,a,b,n;
    Schet* sc;;
    sc=(Schet*)malloc(sizeof(Schet));
    printf("Введите N :");
    scanf("%d",&n);
    for(i=0;i<n;i++){
    printf("Введите а и б : ");
    scanf("%d%d",&a,&b);
    square(a,b,sc);
   // printf("%lf %lf\n",sc.sqr,sc.perim);
   printSchet(sc);
    }
    return 0;
}
Schet* square(int a, int b, Schet* sc){
    double c;
    //printf("1");
//    Schet *sc;
    //sc=(Schet*)malloc(sizeof(Schet));
    c=sqrt(a*a+b*b);
    sc->sqr=(a*b)/2;
    sc->perim=a+b+c;
    //printSchet(sc);
   // printf("%lf %lf %lf\n",c,sc->sqr,sc->perim);
    return sc;
}
void printSchet(Schet * sc){
    printf("%lf %lf\n",sc->sqr,sc->perim);
}
